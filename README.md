<div align="center">

<h1> <a href="https://qiplex.com/software/filmographer/">Filmographer</a> </h1>
  
<h3> Watch all of your movies with a simple click! </h3>

You can get the app on 
<a href="https://gitlab.com/Qiplex-Apps/filmographer/-/releases">GitLab</a>
or on 
<a href="https://qiplex.com/software/Filmographer/">my website</a>
<br>Code comes soon.

![Filmographer](https://qiplex.com/assets/img/app/main/filmographer-app.png)

<h4>Check out the app features below: </h4>

![Filmographer - Features](https://user-images.githubusercontent.com/32670415/147222560-1841cf62-e574-4efd-9463-aff24035e4e2.png)
  
</div>
